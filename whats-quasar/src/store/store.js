/* The code you provided is a JavaScript module that exports an object. This object represents a Vuex
module, which is a state management pattern for Vue.js applications. */

/* The `const state` declaration is creating an object that represents the initial state of the Vuex
module. It contains several properties that will be used to store data in the state. */
const state = {
  contacts: [],
  messages: [],
  chatId: 1,
  active: null,
  openModal: false,
  resendMessage: "",
  userToResendMessage: "",

}

const mutations = {
  /* The `setContacts` mutation is responsible for updating the `contacts` property in the state with
  the provided `payload` value. */
  setContacts(state, payload) {
    state.contacts = payload
  },
  /* The `setMessages` mutation is responsible for updating the `messages` property in the state with
  the provided `payload` value. */
  setMessages(state, payload) {
    state.messages = payload
  },
  /* The `setChatId` mutation is responsible for updating the `chatId` property in the state with the
  provided `payload` value. */
  setChatId(state, payload) {
    state.chatId = payload
  },
  /* The `setActive` mutation is responsible for updating the `active` property in the state with the
  provided `payload` value. This mutation is typically called when the active chat or contact is
  changed. The `payload` value represents the new active chat or contact. By updating the `active`
  property in the state, the application can keep track of the currently active chat or contact and
  use it for various purposes, such as displaying the chat messages or contact details. */
  setActive(state, payload) {
    state.active = payload
  },
  /* The `setOpenModal` mutation is responsible for toggling the value of the `openModal` property in
  the state. It takes the `state` object as a parameter. */
  setOpenModal(state) {
    state.openModal = !state.openModal
  },
  /* The `setResendMessage` mutation is responsible for updating the value of the `resendMessage`
  property in the state. It takes two parameters: `state` and `payload`. */
  setResendMessage(state, payload) {
    state.resendMessage = payload
  },
  /**
   * The function sets the state variable "userToResendMessage" to the value of the payload.
   * @param state - The "state" parameter refers to the current state of the application or module. It
   * is typically an object that contains various properties and values that represent the current
   * state of the application.
   * @param payload - The payload parameter is the data that is being passed to the
   * setuserToResendMessage function. It can be any type of data, such as a string, number, object, or
   * array.
   */
  setuserToResendMessage(state, payload) {
    state.userToResendMessage = payload
  }
}

const actions = {
  /* The `async getContacts(context)` function is an action that is responsible for fetching the
  contacts from the server and updating the state with the retrieved data. */
  async getContacts(context) {

    const resp = await fetch('http://127.0.0.1:8000/api/v1/chats');
    if (resp) {
      const data = await resp.json()
      context.commit('setContacts', data.success)
    }
  },
  /* The `async getMessages(context)` function is an action that is responsible for fetching the
  messages for a specific chat from the server and updating the state with the retrieved data. */
  async getMessages(context) {
    const resp = await fetch(`http://127.0.0.1:8000/api/v1/chats/${context.state.chatId}/messages`);
    if (resp) {
      const data = await resp.json()
      context.commit('setMessages', data.success)
    }
  },
  /* The `async changeChatId(context, id)` function is an action that is responsible for changing the
  active chat ID and fetching the active contact from the server. */
  async changeChatId(context, id) {
    context.commit('setChatId', id);
    const resp = await fetch(`http://127.0.0.1:8000/api/v1/contacts/${context.state.chatId}`);
    if (resp) {
      const data = await resp.json()
      context.commit('setActive', data.success)
    }
  },
  /* The `async sendMessage(context, msg)` function is an action that is responsible for sending a
  message to a specific chat. */
  async sendMessage(context, msg) {
    const data = {
      "msg": msg,
      "status": "RecibidoUsuario",
      "frm": "me"
    }
    const resp = await fetch(`http://127.0.0.1:8000/api/v1/chats/${context.state.chatId}/messages`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    if (resp) {
      return true
    }

  },
  /* The `handleOpenModal` function is an action that is responsible for toggling the value of the
  `openModal` property in the state. It calls the `commit` method on the `context` object to trigger
  the `setOpenModal` mutation, which updates the value of `openModal` in the state. */
  handleOpenModal(context) {
    context.commit('setOpenModal')
  },
  /* The `handleSetResendMessage` action is responsible for updating the value of the `resendMessage`
  property in the state. It takes two parameters: `context` and `message`. */
  handleSetResendMessage(context, message) {
    context.commit('setResendMessage', message)
  },
  /* The `handleSetUserResend` action is responsible for updating the value of the
  `userToResendMessage` property in the state. It takes two parameters: `context` and `id`. */
  handleSetUserResend(context, id) {
    context.commit('setuserToResendMessage', id)
  },
  /* The `async handleResendNewMessage(context)` function is an action that is responsible for
  resending a message to a specific contact. */
  async handleResendNewMessage(context) {
    const data = {
      "contact_id": context.state.userToResendMessage,
      "msg": context.state.resendMessage,
      "status": "Reenviado"
    }
    const resp = await fetch(`http://127.0.0.1:8000/api/v1/messages/resend`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });


    if (resp) {
      return true
    }
  }

}



/* The `export default` statement is used to export the object that represents the Vuex module. In this
case, the object includes the `namespaced`, `state`, `mutations`, and `actions` properties. */
export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
