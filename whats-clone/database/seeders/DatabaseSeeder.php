<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    /**
     * The "run" function calls three different seeders to populate the database with contacts, chats,
     * and messages.
     */
    public function run()
    {
        $this->call([
            ContactsSeeder::class,
            ChatsSeeder::class,
            MessageSeeder::class
        ]);
    }
}
