<?php

namespace Database\Seeders;

use App\Models\Chat;
use App\Models\Contact;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChatsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Chat::create([
            'Contact_id' => 1,
        ]);
        Chat::create([
            'Contact_id' => 2,
        ]);
        Chat::create([
            'Contact_id' => 3,
        ]);

    }
}

