<?php

namespace Database\Seeders;

use App\Models\Chat;
use App\Models\Contact;
use App\Models\Message;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        Message::create([
            'chat_id' => 1,
            'msg' => "Hola",
            'frm' => "me",
            'status' => "Leido",
        ]);
        
        Message::create([
            'chat_id' => 1,
            'msg' => "Hola",
            'frm' => "",
            'status' => "Leido",
        ]);

        Message::create([
            'chat_id' => 1,
            'msg' => "Como estas",
            'frm' => "",
            'status' => "Leido",
        ]);


        Message::create([
            'chat_id' => 2,
            'msg' => "Hola",
            'frm' => "me",
            'status' => "Leido",
        ]);
        Message::create([
            'chat_id' => 2,
            'msg' => "Hola",
            'frm' => "",
            'status' => "Leido",
        ]);

        Message::create([
            'chat_id' => 2,
            'msg' => "Como estas",
            'frm' => "",
            'status' => "Leido",
        ]);


        Message::create([
            'chat_id' => 3,
            'msg' => "Hola",
            'frm' => "me",
            'status' => "Leido",
        ]);
        Message::create([
            'chat_id' => 3,
            'msg' => "Hola",
            'frm' => "",
            'status' => "Leido",
        ]);

        Message::create([
            'chat_id' => 3,
            'msg' => "Como estas",
            'frm' => "",
            'status' => "Leido",
        ]);
    }
}
