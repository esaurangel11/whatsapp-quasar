<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContactsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Contact::create([
            'name' => 'Andrea',
            'celphone' => "0000000000"
        ]);
        Contact::create([
            'name' => 'Esau',
            'celphone' => "0000000000"
        ]);
        Contact::create([
            'name' => 'Magali',
            'celphone' => "0000000000"
        ]);
    }
}

