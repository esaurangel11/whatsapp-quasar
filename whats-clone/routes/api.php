<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ChatsController;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\MessagesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('/v1')->group(function () {
    Route::prefix('/chats')->group(function () {
        Route::get('/', [ChatsController::class, 'create']);
        Route::get('/{chatId}/messages', [MessagesController::class, 'showMessages']);
        Route::post('/{chatId}/messages', [MessagesController::class, 'createMessage']);
    });

    Route::prefix('/contacts')->group(function () {
        Route::get('/', [ContactController::class, 'getContacts']);
        Route::get('/{contact}', [ContactController::class, 'getContactById']);
    });

    Route::prefix('/messages')->group(function () {
        Route::post('/resend', [MessagesController::class, 'resendMessages']);
        Route::get('/{chatId}', [MessagesController::class, 'setMessages']);
    });

});
