<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    const STATUS = ["", "Enviando", "RecibidoServidor", "RecibidoUsuario", "Leido", "Reenviado"];

    protected $fillable = [
        'chat_id',
        'msg',
        'frm',
        'status',
        'created_at',
        'updated_at'
    ];

}
