<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'contact_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    //RELATIONS

    /**
     * The function "contact" returns a relationship between the current object and a Contact model.
     *
     * @return a relationship between the current model and the Contact model.
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    /**
     * The function returns a collection of messages associated with the current object.
     *
     * @return a collection of messages.
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }


    //SCOPES
    /**
     * The scopeLastChat function is used to retrieve the last chat based on the latest message's
     * created_at timestamp.
     *
     * @param query The `` parameter is an instance of the query builder class. It allows you to
     * build and execute database queries.
     *
     * @return a query builder instance.
     */
    public function scopeLastChat($query)
    {
        return $query->whereHas("messages")
            ->orderBy(function ($query) {
                $query->select("created_at")
                    ->from("messages")
                    ->whereColumn("chat_id", "chats.id")
                    ->orderBy("created_at", "desc")
                    ->limit(1);
            }, "desc");
    }
}
