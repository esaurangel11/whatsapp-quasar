<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use Illuminate\Http\Request;
use App\Models\Message;

class MessagesController extends Controller
{
    /**
     * The function retrieves and returns all messages associated with a specific chat ID in ascending
     * order of their creation time.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains information about the request such as the request
     * method, headers, and request data.
     *
     * @return a JSON response with the "success" key and the value being the messages retrieved from
     * the database.
     */
    public function showMessages(Request $request)
    {

        $chatId = $request->route('chatId');
        $messages = Message::where('chat_id', $chatId)->orderBy('created_at', 'asc')->get();

        return response()->json(["success" => $messages]);
    }

    /**
     * The createMessage function in PHP validates a request, creates a new message with the provided
     * data, and returns a JSON response with the success status and the created message.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains all the information about the request, such as the
     * request method, headers, and input data.
     *
     * @return a JSON response with the success status and the created message.
     */
    public function createMessage(Request $request)
    {
        request()->validate([
            'msg' => 'required|string|min:1|max:50',
            'status' => 'required|string|in:'.join(",", Message::STATUS),

        ]);

        $chatId = $request->route('chatId');



        $message = Message::create([
            'msg' => request()->msg,
            'chat_id' => $chatId,
            'frm' => request()->frm,
            'status' => "RecibidoUsuario"
        ]);

        return response()->json(['success' => $message]);
    }

    /**
     * The function `resendMessages` validates the request parameters, creates a new message with the
     * provided data, and returns a JSON response indicating success along with the created message.
     *
     * @return a JSON response with the success status and the created message object.
     */
    public function resendMessages()
    {
        request()->validate([
            'contact_id' => 'required|integer|exists:contacts,id',
            'msg' => 'required|string|min:5|max:100',
            'status' => 'required|string|in:'.join(",", Message::STATUS)
        ]);


        $message = Message::create([
            "chat_id" => request()->contact_id,
            "msg" => request()->msg,
            "frm" => "me",
            "status" => request()->status
        ]);

        return response()->json(['success' => $message]);
    }

    /**
     * The function sets the status of all messages in a chat to "Leido" and returns a success message.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains information about the request such as the request
     * method, headers, and request data.
     *
     * @return a JSON response with the key "success" and the value "exito".
     */
    public function setMessages(Request $request)
    {
        $chatId = $request->route('chatId');
        $message = Message::where("chat_id", $chatId)->update(["status" => "Leido"]);
        return response()->json(["success" => "exito"]);
    }
}
