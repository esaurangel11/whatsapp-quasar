<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatsController extends Controller
{

    /**
     * The above code is a PHP function that retrieves the last chat and its messages, and returns them
     * as a JSON response.
     *
     * @return The create() function returns a JSON response containing the "success" key with the
     * value of the  variable.
     */
    public function create()
    {

        $chats = Chat::lastChat()->with(["contact"])->get();

        return response()->json(['success' => $chats]);
    }

    /**
     * The function retrieves chat messages for a specific chat ID, including the contact information
     * and ordering the messages by their creation date.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains information about the request such as the request
     * method, headers, and input data.
     *
     * @return a JSON response with the "success" key and the value of the  variable.
     */
    public function showMessages(Request $request)
    {
        $chatId = $request->route('chatId');


        $messages = Chat::where("id", $chatId)->with(["contact", "messages" => function ($q) {
            $q->orderBy("created_at", "desc");
        }])
            ->whereHas("messages")
            ->get();
        return response()->json(["success" => $messages]);
    }
}
