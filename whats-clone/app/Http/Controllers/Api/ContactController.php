<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
   /**
    * The function retrieves contacts from the database and returns them as a JSON response.
    *
    * @return a JSON response containing the contacts data. The contacts are retrieved from the
    * database, ordered by name in ascending order, and paginated based on the specified number of
    * contacts per page. The success key in the JSON response contains the paginated contacts data.
    */
    public function getContacts(){

        $per_page = request()->per_page ? request()->per_page : 10;

        $contacts = Contact::orderBy('name', 'asc')->paginate($per_page);

        return response()->json(['success' => $contacts]);
    }

    /**
     * The function returns a JSON response with the contact information if it exists, otherwise it
     * returns an error message.
     *
     * @param Contact contact The parameter "contact" is of type "Contact", which means it expects an
     * object of the class "Contact" to be passed as an argument to the function.
     *
     * @return a JSON response. If the  object exists, it will return a success response with
     * the  object as the data. If the  object does not exist, it will return an error
     * response with a message indicating that the resource does not exist.
     */
    public function getContactById(Contact $contact){

        if($contact){
            return response()->json(["success" => $contact]);
        }else{
            return response()->json(["error" => ["msg" => "No existe el recurso."]]);
        }
    }
}
